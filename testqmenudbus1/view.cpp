#include "view.h"

#include <QDBusConnection>
#include <QDBusMessage>
#include <QMenu>

view::view(QWidget *parent) :
    QGraphicsView(parent)
{

}

void view::mouseDoubleClickEvent(QMouseEvent * event)
{
    QDBusMessage m = QDBusMessage::createMethodCall("org.TestQMenu",
                                                    "/TestQMenu",
                                                    "local.MainWindow",
                                                    "showMenu");
    QDBusConnection::sessionBus().send(m);
    QGraphicsView::mouseDoubleClickEvent(event);
}

void view::mouseReleaseEvent(QMouseEvent * event)
{
    // uncomment to test single click
//    QDBusMessage m = QDBusMessage::createMethodCall("org.TestQMenu",
//                                                    "/TestQMenu",
//                                                    "local.MainWindow",
//                                                    "showMenu");
//    QDBusConnection::sessionBus().send(m);
}
