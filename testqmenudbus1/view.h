#ifndef VIEW_H
#define VIEW_H

#include <QObject>
#include <QGraphicsView>

class view : public QGraphicsView
{
public:
    view(QWidget *parent = nullptr);
    void mouseDoubleClickEvent(QMouseEvent * event) override;
    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // VIEW_H
