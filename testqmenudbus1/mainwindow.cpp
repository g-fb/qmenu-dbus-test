#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "view.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    view *v = new view();
    setCentralWidget(v);
}

MainWindow::~MainWindow()
{
    delete ui;
}
