#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "view.h"
#include <testqmenudbux_adaptor.h>

#include <QDBusConnection>
#include <QDBusConnectionInterface>

static QDBusConnection connection(QLatin1String(""));

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // uncomment to test the menu in the same app
//    view *v = new view();
//    setCentralWidget(v);

    new MainWindowAdaptor(this);
    QDBusConnection dbus = QDBusConnection::sessionBus();
    dbus.registerObject("/TestQMenu", this);
    dbus.registerService("org.TestQMenu");

    menu = new QMenu(this);
    menu->setFixedWidth(300);
    menu->addAction("test");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showMenu()
{
    QThread::msleep(150);
    menu->exec(QCursor::pos());
}
